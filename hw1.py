# Nathan Welch
# Data Mining Hw 1
# 9/5/18
import random

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

pd.set_option('display.expand_frame_repr', False)
pd.set_option('display.max_rows', 1400)

train_df = pd.read_csv('train.csv')
test_df = pd.read_csv('test.csv')
combine = [train_df, test_df]

all = pd.concat(combine, sort=False)

# all = pd.read_csv('all.csv')


print('\n\n------ Question 7 -----------------------')
print(all.describe())

print('\n\n------ Question 8 -----------------------')
print(all.describe(include=[np.object]))

print('\n\n------ Question 9 -----------------------')
# first = category.groupby(['Pclass'])
print(all[['Pclass', 'Survived']].groupby(['Pclass'], as_index=False).mean())


print('\n\n------ Question 10 -----------------------')
print(all[['Sex', 'Survived']].groupby(['Sex'], as_index=False).mean())

print('\n\n------ Question 11 -----------------------')

dead = pd.DataFrame({
    'Dead': all.groupby('Survived').get_group(0).Age
    }).plot.hist(bins=20)

alive = pd.DataFrame({
    'Alive': all.groupby('Survived').get_group(1).Age
    }).plot.hist(bins=20)

dead.set(xlabel='Age', ylabel='Survived', title='Died', ylim=[0, 60])
alive.set(xlabel='Age', ylabel='Survived', title='Lived', ylim=[0, 60])
# plt.show()


print('\n\n------ Question 12 -----------------------')

g = sns.FacetGrid(all, col="Survived",  row="Pclass")
g = g.map(plt.hist, "Age", bins=20, color="r")
# plt.show()


print('\n\n------ Question 13 -----------------------')

grid = sns.FacetGrid(train_df, row='Embarked', col='Survived')
grid.map(sns.barplot, 'Sex', 'Fare', ci=None)
grid.add_legend()
# plt.show()


print('\n\n------ Question 16 -----------------------')

data = pd.DataFrame(all['Sex'])
print('Before Conversion: \n', data.head())

data['Sex'] = data['Sex'].apply({'male':0, 'female':1}.get)
print('After Converting to numbers: \n', data.head())

all['Gender'] = data['Sex']
print(all.head())   # just to check
# all.to_csv('FF_data_frame.csv')   # this will change the actual file


print('\n\n------ Question 17 -----------------------')
# fill in the NaN places with a random number inbetween standard deviation and the mean
# all['Age'] = all['Age'].fillna(random.randint(14, 30))
def rand_works():
    return random.randint(14, 30)

table = all
table = table.reset_index()
table.Age = table.Age.replace(np.nan, 400, regex=True)
for i, j in table.iterrows():
    if j.Age == 400:
        table.loc[i, 'Age'] = rand_works()


print('\n\n------ Question 18 -----------------------')
most = all.Embarked.value_counts().index[0]
print('Most used value:', most)
print(type(most))
data = pd.DataFrame(all['Embarked'])
print('Data Before:\n',  data.Embarked[[828, 829, 830]])
data['Embarked'] = data['Embarked'].fillna(most)
print('Data After:\n', data.Embarked[[828, 829, 830]])


print('\n\n------ Question 19 -----------------------')
mode = all.Fare.mode()[0]
print('Most used value:', mode)
data = pd.DataFrame(all['Fare'])
data['Fare'] = data['Fare'].fillna(mode)
print(data.describe())

print('\n\n------ Question 20 -----------------------')

for i, j in all.iterrows():
    if j.Fare <= 7.91:
        all.loc[i, 'Fare'] = 0
    if j.Fare > 7.91 and j.Fare <= 14.454:
        all.loc[i, 'Fare'] = 1
    if j.Fare > 14.454 and j.Fare <= 31.0:
        all.loc[i, 'Fare'] = 2
    if j.Fare > 31.0:
        all.loc[i, 'Fare'] = 3

print(all)
